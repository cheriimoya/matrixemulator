import pdb

class Emulator:

    standardRowMapping = [9, 14, 8, 12, 1, 7, 2, 5]
    standardColMapping = [13, 3, 4, 10, 6, 11, 15, 16]

    def __init__(self):
        self.rows = [0]*8
        self.cols = [0]*8

    def setPin(self, pin, state, dump=False):
        if (pin in self.standardRowMapping):
            self.rows[self.standardRowMapping.index(pin)] = state
        else:
            self.cols[self.standardColMapping.index(pin)] = state

        if (dump):
            self.dumpMatrix()


    def dumpMatrix(self):
        for y in self.rows:
            for x in self.cols:
                print(y*x, end='')
            print('\n', end='')

